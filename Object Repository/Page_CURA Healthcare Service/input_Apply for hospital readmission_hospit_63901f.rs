<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Apply for hospital readmission_hospit_63901f</name>
   <tag></tag>
   <elementGuidId>a50f4e8e-dd19-4f32-a827-a486811ca3a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#chk_hospotal_readmission</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk_hospotal_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>450ea041-1298-4ada-a288-6a0e3a770fd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>1fd7b6f2-cb89-481b-98f9-5d0a44b4e45c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>013070cb-6ef6-4c31-af0d-cd285aca01cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>fdd1f526-842a-4668-a0e6-0d810e01c286</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>5d6dfb65-4f49-4692-87ee-63b5039f1e61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk_hospotal_readmission&quot;)</value>
      <webElementGuid>78693ab1-d355-4879-9562-10e3f7a942cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk_hospotal_readmission']</value>
      <webElementGuid>d6e24214-ca38-4064-a4bc-fdc90aa9c615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label/input</value>
      <webElementGuid>15f42425-ca2c-4bd7-88c8-f6a5a800cd67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>6a632326-d53c-4c56-80f9-6058e5692217</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk_hospotal_readmission' and @name = 'hospital_readmission']</value>
      <webElementGuid>1db99fa1-9b2c-4012-ba0c-4fcd45aebb04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
